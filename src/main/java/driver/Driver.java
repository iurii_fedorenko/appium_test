package driver;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class Driver {

    private static AndroidDriver driver;

    private Driver(){}

    public static AndroidDriver getDriverInstance(){
        if (!isInstanceExist()) {
            try {
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability("deviceName","08700afb00d3e4de");
                capabilities.setCapability("platformName","Android");
                capabilities.setCapability("app", "D:\\Trainings\\Appium\\apps\\base.apk");
                driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
            } catch (MalformedURLException e){
                e.printStackTrace();
            }
        } else {
            return driver;
        }
        return driver;
    }

    public static boolean isInstanceExist(){
        if (driver != null){
            return true;
        } else
            return false;
    }


    public static void killWebDriverInstance() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }
}
