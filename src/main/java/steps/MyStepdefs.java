package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;
import ui.screens.MainScreen;

public class MyStepdefs {

    @Given("^User launch the app$")
    public void openApp() {
        new MainScreen().openApp();
    }

    @When("^User tap on the 'Military' tab$")
    public void userTapOnTheMilitaryTab(){
        new MainScreen().openMilitaryTab();
    }

    @Then("^'Military' tab is open$")
    public void militaryTabIsOpen(){
        Assert.assertTrue(new MainScreen().militaryTabIsOpen(), "Military tab is not open");
        System.out.println("Military tab is open");
    }


}
