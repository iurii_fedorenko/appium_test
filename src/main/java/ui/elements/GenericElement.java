package ui.elements;

import driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class GenericElement {
    private static final int WAIT_FOR_ELEMENT_TIMEOUT_SECONDS = 30;
    protected WebDriver driver;
    protected By elementLocator;


    public GenericElement(By by) {
        elementLocator = by;
    }

    public boolean isElementPresentAndSingle() {
        int elementQtty = Driver.getDriverInstance().findElements(elementLocator).size();
        switch (elementQtty){
            case 1:
                return true;
            case 0: {
                System.out.println("Element with locator: " + elementLocator.toString() + " not found");
                return false;
            }
            default: {
                System.out.println("More than one elements were found by locator: " + elementLocator.toString());
                return false;
            }
        }
    }

    public boolean isElementEnabled(){
        return isElementPresentAndSingle()&&driver.findElement(elementLocator).isEnabled();
    }

    public boolean isElementSelected(){
        return isElementPresentAndSingle()&&driver.findElement(elementLocator).isSelected();
    }

    protected void waitElementPresent(){
        if (!isElementEnabled())
            new WebDriverWait(driver, WAIT_FOR_ELEMENT_TIMEOUT_SECONDS).until(ExpectedConditions.elementToBeSelected(elementLocator));
    }

    public void clickElement(){
        //waitElementPresent();
        Driver.getDriverInstance().findElement(elementLocator).click();
    }

    public void clickElementThrowJS(){
        waitElementPresent();
        WebElement element = driver.findElement(elementLocator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", element);
    }

    public void findElementFromListAndClick(String attributeName, String attributeValue){
        if(driver.findElements(elementLocator).size()!= 0){
            for(WebElement element: driver.findElements(elementLocator)){
                if(element.getAttribute(attributeName).equals(attributeValue))
                    element.click();
            }
        }
    }
}
