package ui.screens;

import driver.Driver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractScreen {

    private static final int WAIT_FOR_ELEMENT_TIMEOUT_SECONDS = 30;

    protected void openPage(String pageURL){
        if (!isPageOpen(pageURL)) {
            Driver.getDriverInstance().get(pageURL);
            waitPageLoading(pageURL);
        }
    }

    protected boolean isPageOpen (String pageURL){
        return Driver.getDriverInstance().getCurrentUrl().equals(pageURL);
    }

    protected void waitPageLoading (String pageURL){
        new WebDriverWait(Driver.getDriverInstance(), WAIT_FOR_ELEMENT_TIMEOUT_SECONDS).until(ExpectedConditions.urlContains(pageURL));
    }
}
