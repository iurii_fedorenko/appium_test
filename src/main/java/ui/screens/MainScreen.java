package ui.screens;

import driver.Driver;
import org.openqa.selenium.By;
import ui.elements.GenericElement;

public class MainScreen extends AbstractScreen {
    private static final By MILITARY_TAB = By.xpath("//android.widget.TextView[@text='Military']");
    private static final By POINT_LABEL = By.xpath("//android.widget.TextView[@text='POINTS']");

    public void openMilitaryTab (){
        new GenericElement(MILITARY_TAB).clickElement();
    }

    public void openApp(){
        Driver.getDriverInstance();
    }

    public boolean militaryTabIsOpen(){
        return new GenericElement(POINT_LABEL).isElementPresentAndSingle();
    }
}
