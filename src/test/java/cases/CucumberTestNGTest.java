package cases;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import driver.Driver;
import org.testng.annotations.AfterClass;

@CucumberOptions(
        strict = true,
        monochrome = true,
        glue = {"steps"},
        features="src/test/resources/app.feature",
            plugin = { "json:target/cucumber-report.json",
                    "html:target/cucumber-report"
            }
)
public class CucumberTestNGTest extends AbstractTestNGCucumberTests {

    @AfterClass
    public void closeBrowser(){
        Driver.killWebDriverInstance();
    }
}
