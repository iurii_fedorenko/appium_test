$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/app.feature");
formatter.feature({
  "line": 1,
  "name": "Click on a tab",
  "description": "",
  "id": "click-on-a-tab",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Open new tab",
  "description": "",
  "id": "click-on-a-tab;open-new-tab",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "User launch the app",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User tap on the \u0027Military\u0027 tab",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "\u0027Military\u0027 tab is open",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.openApp()"
});
formatter.result({
  "duration": 35146771035,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.userTapOnTheMilitaryTab()"
});
formatter.result({
  "duration": 440276001,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.militaryTabIsOpen()"
});
formatter.result({
  "duration": 779832394,
  "status": "passed"
});
});